use strict;
my ($featureType,$bioType) = @ARGV;
while(<STDIN>){
  chomp;
  next if(/^\s*#/);
  my $line = $_;
  my @F=split/\t/,$line;
  my $name = "NULL";
  next if($F[2] ne $featureType);
  if(defined $bioType){
    if($line=~ /gene_biotype.*?\"([a-zA-Z0-9_]+)\"/){
      next if($1 ne $bioType);
    }
  }
  if($line =~ /gene_id.*?\"([A-Z0-9]+)\"/){
    $name =$1;
  }
  print join "\t", ($F[0],$F[3],$F[4],"10",$F[6],$name);
  print "\n";
}
