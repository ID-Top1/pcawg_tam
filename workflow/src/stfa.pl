use strict;
my ($prefix,$lin,$go,@ti,$tii);
if ($ARGV[0]){
	if ($ARGV[0] =~ /\w+/){
		$prefix = $ARGV[0];
	}
}

my $type = "stfa";

while (<STDIN>){
	chomp;
	$lin = $_;
	if ($lin =~ /^>\w+/){
		$go++;
		@ti = split /\s+/, $lin;
		$tii = $ti[0];
		$tii =~ s/^>//;
		$tii =~ s/\//:/g;
		open (FO, ">$prefix.$tii.$type") or die "Failed to write output file: $!\n";
		print FO "$lin\n";
	}
	elsif ($go >= 1){
		if ($lin =~ /^\S+/){
			print FO "$lin\n";
		}
		else { $go = 0; close FO;}
	}
}

1;
