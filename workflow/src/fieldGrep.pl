use strict;
my ($field,$pattern,$sep) = @ARGV;
$sep = "\t" if(!defined $sep);
while (<STDIN>){
  chomp;
  my @sp = split /$sep/;
  if(defined $sp[$field]){
    if($sp[$field]=~/$pattern/){
      print "$_\n";
    }
  }
}
