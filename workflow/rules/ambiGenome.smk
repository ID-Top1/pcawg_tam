CHROMS = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,"X"]
DIS = ["Ovary-AdenoCA","CNS-PiloAstro","Liver-HCC","Panc-Endocrine","Kidney-RCC","Prost-AdenoCA","Lymph-BNHL","Panc-AdenoCA","Eso-AdenoCa","CNS-Medullo","Lymph-CLL","Skin-Melanoma","Stomach-AdenoCA","Breast-AdenoCa","Head-SCC","Lymph-NOS","Myeloid-AML","Biliary-AdenoCA","Bone-Osteosarc","Breast-DCIS","Breast-LobularCa","Myeloid-MPN","Myeloid-MDS","Bone-Cart","Bone-Epith"]
TIS = ["brain","breast","esophagus","intestine","kidney","liver","lung","ovary","pancreas","prostate","skeletal","skin","stomach","thyroid","bladder","cervix","uterus"]

rule downloadGenome:
  output:
    refout=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa.gz.defunct"
  resources:
    mem_mb=4000,
    resources='"rusage[mem=4000]"',
    time="4:00"
  params:
    dir=os.environ['projectRoot']+config['genomeDir'],
    genomeFa="{genome}.fa.gz",
    url=config['genomeUrl'],
  log:
    o=os.environ['projectRoot']+config['logsDir']+"getGenome.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"getGenome.{genome}.e"
  shell:
    """
    wget -nd {params.url};
    wget -nd http://ftp.ensembl.org/pub/grch37/current/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz
    ln -s Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz GRCh37.fa.gz
    wget -nd http://ftp.ensembl.org/pub/release-104/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
    ln -s Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz GRCh38.fa.gz
    wget -nd http://ftp.ensembl.org/pub/grch37/current/gtf/homo_sapiens/Homo_sapiens.GRCh37.87.gtf.gz
    ln -s Homo_sapiens.GRCh37.87.gtf.gz GRCh37.gtf.gz
    wget -nd http://ftp.ensembl.org/pub/release-104/gtf/homo_sapiens/Homo_sapiens.GRCh38.104.gtf.gz
    ln -s Homo_sapiens.GRCh38.104.gtf.gz GRCh38.gtf.gz
    gunzip < GRCh37.fa.gz > GRCh37.fa
    gunzip < GRCh38.fa.gz > GRCh38.fa
    """

rule stfaGenome:
  input:
    gz=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa.gz"
  output:
    refout="{genome}.md5sum"
  resources:
    mem_mb=4000,
    resources='"rusage[mem=4000]"',
    time="4:00"
  params:
    stfa=os.environ['projectRoot']+config['srcDir']+"stfa.pl",
    genome="{genome}"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"stfaGenome.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"stfaGenome.{genome}.e"
  shell:
    """
    gunzip < {input.gz} | perl -w {params.stfa} {params.genome} ;
    md5sum {input.gz} > {output.refout}
    """

rule ambiAlignGenome:
  input:
    md5="{genome}.md5sum",
    #fa="{genome}.{chr}.stfa"
  output:
    refout="{genome}.{chr}.ambiSummary.Rtab",
    pickle="{genome}.{chr}.ambiPickle.Rdat"
  resources:
    mem_mb=80000,
    resources='"rusage[mem=80000]"',
    time="4:00"
  params:
    rscript=os.environ['projectRoot']+config['srcDir']+"prepGenome.R",
    genome="{genome}",
    fa="{genome}.{chr}.stfa"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"stfaGenome.{genome}.{chr}.o",
    e=os.environ['projectRoot']+config['logsDir']+"stfaGenome.{genome}.{chr}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {params.fa} {output.pickle} {output.refout} 1> {log.o} 2> {log.e};
    """

rule collectPickles:
  input:
    expand("{{genome}}.{chr}.ambiSummary.Rtab",chr=CHROMS)
  output:
    refout="{genome}.branston"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"collectPickles.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"collectPickles.{genome}.e"
  shell:
     """
     date > {output.refout}
     """

rule biasedExpression:
  input:
    mutations="icgc.pcawg.del"
  output:
    refout="biasedExp.{disease}.{tissue}.Rtab"
  resources:
    mem_mb=10000,
    resources='"rusage[mem=10000]"',
    time="4:00"
  params:
    rscript=os.environ['projectRoot']+config['srcDir']+"biasedExpressionAnalysis.R",
    tt="{tissue}",
    de="{disease}"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"biasedExp.{disease}.{tissue}.o",
    e=os.environ['projectRoot']+config['logsDir']+"biasedExp.{disease}.{tissue}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {params.tt} {params.de} > {log.o} 2> {log.e}
    """

rule biasedExpressionRunner:
  input:
    expand("biasedExp.{disease}.{tissue}.Rtab",disease=DIS,tissue=TIS)
  output:
    refout="biasedExp.complete"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"be.o",
    e=os.environ['projectRoot']+config['logsDir']+"be.e"
  shell:
    """
    date > {output.refout}
    """
