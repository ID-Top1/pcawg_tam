
rule readyGenomeOne:
  input:
    fa="{genome}.fa.gz",
    gtf="{genome}.gtf.gz"
  output:
    fa="{genome}.fa",
    index="{genome}.fa.fai",
    pilot="{genome}.pilot10.bed"
  params:
    stripCols="perl -w "+os.environ['projectRoot']+config['srcDir']+"stripCols.pl",
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="01:00"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"ready.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"ready.{genome}.e"
  shell:
    """
    set +o pipefail;
    zcat {input.gtf} | grep -v '#' | head -10 | {params.stripCols} > {output.pilot};
    zcat {input.fa} > {output.fa};
    bedtools getfasta -fi {output.fa} -bed {output.pilot} > /dev/null;
    """

rule geneStructures:
  input:
    gtf="{genome}.gtf.gz"
  output:
    bedGenic="{genome}.genic.bed",
    bedExons="{genome}.exons.bed",
    bedIntrons="{genome}.introns.bed",
    bedCds="{genome}.cds.bed",
    refout="{genome}.genicWindows.bed"
  params:
    perlScript=os.environ['projectRoot']+config['srcDir']+"getTranscriptEnds.pl",
    genome=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa.fai",
    stripCols="perl -w "+os.environ['projectRoot']+config['srcDir']+"stripCols.pl",
    justGenome="{genome}"
  resources:
    mem_mb=4000,
    resources='"rusage[mem=4000]"',
    time="02:00"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"geneStructures.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"geneStructures.{genome}.e"
  shell:
    """
    set +o pipefail;
    zcat {input} | {params.stripCols} gene protein_coding > {output.bedGenic};
    zcat {input} | {params.stripCols} exon protein_coding > {output.bedExons};
    bedtools subtract -a {output.bedGenic} -b {output.bedExons} -s  > {output.bedIntrons};
    zcat {input} | {params.stripCols} CDS protein_coding > {output.bedCds};
    bedtools makewindows -b {output.bedGenic} -w 50 -s 50 -i srcwinnum > {output.refout}
    """

rule icgcGenes:
  output:
    refout="Homo_sapiens.{genome}.{ensver}.gtf",
    dels="icgc.pcawg.{genome}.{ensver}.del"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="04:00"
  params:
    url="http://ftp.ensembl.org/pub/release-{ensver}/gtf/homo_sapiens/Homo_sapiens.{genome}.{ensver}.gtf.gz",
    icgcurl="https://dcc.icgc.org/api/v1/download?fn=/PCAWG/consensus_snv_indel/final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz",
    perlscript=os.environ['projectRoot']+config['srcDir']+"fieldGrep.pl"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"icgcGenes.{genome}.{ensver}.o",
    e=os.environ['projectRoot']+config['logsDir']+"icgcGenes.{genome}.{ensver}.e"
  shell:
    """
    wget -nd {params.url} -O {output.refout}.gz;
    pigz -d {output.refout}.gz ;
    wget -nd {params.icgcurl} -O final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz;
    pigz -d final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz;
    head -1 final_consensus_passonly.snv_mnv_indel.icgc.public.maf > final_consensus_passonly.snv_mnv_indel.icgc.public.maf.header ;
    grep DEL final_consensus_passonly.snv_mnv_indel.icgc.public.maf | perl {params.perlscript} 6 DEL '\t' > final_consensus_passonly.snv_mnv_indel.icgc.public.maf.del ;
    cat final_consensus_passonly.snv_mnv_indel.icgc.public.maf.header final_consensus_passonly.snv_mnv_indel.icgc.public.maf.del > {output.dels} ;
    ln -s {output.dels} icgc.pcawg.del
    """
