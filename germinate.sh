# Assumes there is an installed and initialised version of conda and mamba
# you can substitute conda for mamba below but dependency resolution can be
# painfully slow with conda.
export base=`pwd`
export projectRoot=${base}/
mamba env create --prefix ${projectRoot}workflow/root -f ${projectRoot}workflow/envs/root.yaml
conda activate ${projectRoot}workflow/root
