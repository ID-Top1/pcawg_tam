# pcawg_tam

Pan-cancer analysis of deletion mutation patterns relative to genic transcription.

**Installation**

This analysis code assumes a pre-existing install of Conda and ideally Mamba to bootstrap the working environment. Clone the project into a new location and move into the top-level directory of the project.



Build the environment (only needs to be done on first use)
~~~
$ source ./germinate.sh
~~~

Intialise the envirnoment (done from within the project top-level directory)
~~~
$ source ./init.sh
~~~

Move to the analysis directory
~~~
$ cd analysis/ribo
~~~

Run the default analysis pipeline
~~~
$ snakemake --cores 2
~~~

**Data**

All data required for the analysis should be pulled in automatically. Code in the workflow/rules directory choreographs the work of the pipeline. Resource parameters (e.g. memory and time) have been included in the Snakemake rules so they can be passed to schedullers in a batch system, e.g. SGE, LSF etc.
