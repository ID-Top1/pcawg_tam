# Assumes there is an installed and initialised version of conda
export base=`pwd`
export projectRoot=${base}/
export logs=${base}/logs
conda config --set env_prompt '({name})'
conda activate ${base}/workflow/root
